import MyArray from '../index';

describe('tests for method splice', () => {
  let arr = new MyArray();
  beforeEach(() => {
    arr = new MyArray(1, 2, 3, 4, 5, 6);
  });
  test('should mutate original array if method has 3 parameters', () => {
    arr.splice(2, 0, 777);
    expect(arr).not.toEqual(new MyArray(1, 2, 3, 4, 5, 6));
  });
  test('should return an array containing the deleted elements. ', () => {
    const b = arr.splice(0, 2);
    expect(b[0]).toBe(1);
    expect(b[1]).toBe(2);
  });
  test('an empty array is returned, if no elements are removed.', () => {
    expect(arr.splice(0, 0)).toHaveLength(0);
  });
  test('an empty array is returned, if second argument is negative.', () => {
    expect(arr.splice(0, -1)).toHaveLength(0);
  });
  test('starting from the first parameter deleted all elements if second parameter is more than length array.', () => {
    arr.splice(1, 15);
    expect(arr).toHaveLength(1);
  });
  test('if 2 arguments and the first one more than length of array - do nothing and return an empty array', () => {
    expect(arr.splice(10, 1)).toHaveLength(0);
  });
  test('should point on element from the end, if the first index is negative', () => {
    const b = arr.splice(-2, 1);
    expect(b[0]).toBe(5);
  });
  test('should сut elements of array, from position declareted as first arg', () => {
    const a = arr.splice(-2);
    expect(a[0]).toBe(5);
    expect(a[1]).toBe(6);
    const b = arr.splice(2);
    expect(b[0]).toBe(3);
    expect(b[1]).toBe(4);
    const c = arr.splice(0);
    expect(c[0]).toBe(1);
    expect(c[1]).toBe(2);
  });
  test('should change original length of array', () => {
    const len = arr.length;
    arr.splice(1, 1);
    expect(arr.length).not.toBe(len);
  });
  test(`should change length of original array, if second parameter <== 0 
  returns empty array, and accepts the first parameter to start.`, () => {
    const item = arr.splice(2, -999, 'H', 'e', 'y');
    expect(item).toHaveLength(0);
    expect(arr).toHaveLength(9);
    expect(arr[2]).toBe('H');
  });
  test('should return empty array without changing original array dosn\'t put any arguments', () => {
    expect(arr.splice()).toHaveLength(0);
  });
  test('instance has not Own Property splice', () => {
    expect(Object.prototype.hasOwnProperty.call(arr, 'splice')).toBeFalsy();
  });
  test('result array should be instance of array class', () => {
    expect(arr.splice).toBeInstanceOf(Function);
  });
});
