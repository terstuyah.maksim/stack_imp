import MyArray from '../index';

describe('tests for shift method', () => {
  test('instance has not Own Property shift', () => {
    const arr = new MyArray();

    expect(Object.prototype.hasOwnProperty.call(arr, 'shift')).toBeFalsy();
  });

  test('instance has method shift', () => {
    const arr = new MyArray();
    expect(arr.shift).toBeInstanceOf(Function);
  });

  test('should decrease array length by 1', () => {
    const arr = new MyArray(1, 2, 3);
    arr.shift();
    expect(arr).toHaveLength(2);
  });

  test('should shift the elements at consecutive indexes down', () => {
    const arr = new MyArray('a', 'b', 'c');
    arr.shift();

    expect(arr[0]).toEqual('b');
    expect(arr[1]).toEqual('c');
  });

  test('return removed element', () => {
    const arr = new MyArray('a', 'b', 'c');

    expect(arr.shift()).toBe('a');
  });

  test('should return undefined for empty array', () => {
    const arr = new MyArray();
    expect(arr.shift()).toBeUndefined();
  });

  test('should not change array if length equal to 0', () => {
    const arr = new MyArray();
    arr.shift();
    expect(arr).toHaveLength(0);
  });

  test('should not have required arguments', () => {
    const arr = new MyArray();
    expect(arr.shift).toHaveLength(0);
  });

  test('should work with NaN, null, undefined', () => {
    const arr = new MyArray(NaN, null, undefined);
    expect(arr.shift()).toBeNaN();
    expect(arr.shift()).toBeNull();
    expect(arr.shift()).toBeUndefined();
  });
});
