import MyArray from '../index';

describe('tests for unshift method', () => {
  test('instance should not have Own Property unshift', () => {
    const arr = new MyArray();

    expect(Object.prototype.hasOwnProperty.call(arr, 'unshift')).toBeFalsy();
  });

  test('instance should have a method unshift', () => {
    const arr = new MyArray();
    expect(arr.unshift).toBeInstanceOf(Function);
  });

  test('should add all arguments from method in the same order and increase array length', () => {
    const arr = new MyArray();
    arr.unshift(1, 2, 3);
    expect(arr).toHaveLength(3);
    expect(arr).toEqual(new MyArray(1, 2, 3));
  });

  test('should not change array and array.length without arguments', () => {
    const arr = new MyArray(1, 2);
    arr.unshift();
    expect(arr[0]).toBe(1);
    expect(arr).toHaveLength(2);
  });

  test('should have one required argument', () => {
    const arr = new MyArray();
    expect(arr.unshift).toHaveLength(1);
  });
});
